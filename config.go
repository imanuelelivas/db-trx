package dbtrx

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"
)

// Config is a
type Config struct {
	DbName     string `json:"db_name"`
	DbUsername string `json:"db_username"`
	DbPassword string `json:"db_pasword"`
	DbHost     string `json:"db_host"`
}

func config() (Config, error) {
	var config Config

	file, err := ioutil.ReadFile("db_trx_config.json")
	if err != nil {
		return config, fmt.Errorf("Fail Read file db_trx_config.json. Error :  %s", err)
	}

	err = json.Unmarshal(file, &config)
	if err != nil {
		return config, err
	}

	return config, nil
}

// ConnectDB is a
func ConnectDB() (*sql.DB, error) {
	config, err := config()
	if err != nil {
		return nil, err
	}
	DBUsername := config.DbUsername
	DBPassword := config.DbPassword
	DBHost := config.DbHost
	DBName := config.DbName
	// URI := "briopr:jakarta123@tcp(10.35.65.136:3306)/mocash"
	URI := fmt.Sprintf("%s:%s@tcp(%s)/%s", DBUsername, DBPassword, DBHost, DBName)

	db, err := sql.Open("mysql", URI)
	if err != nil {
		return nil, err
	}

	return db, nil
}

// Timestamp is format tanggal
func Timestamp() string {
	currentTime := time.Now()
	Timestamp := currentTime.Format("2006-01-02 15:04:05")
	return Timestamp
}
