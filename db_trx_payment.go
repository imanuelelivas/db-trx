package dbtrx

import (
	"database/sql"
	"fmt"
	"log"

	// _ is driver mysql
	_ "github.com/go-sql-driver/mysql"
)

// FieldTrxPayment adalah kolom yang ada pada tabel tbl_bw_trx_payment
type FieldTrxPayment struct {
	ReferenceNumber string
	PaymentType     string
	Status          string
	Account         string
	PaymentNumber   string
	PaymentName     string
	Amount          string
	TrxScheduleType string
	ThirdPartyName  string
	AccountType     string
	F1              string
	F2              string
	F3              string
	F4              string
	F5              string
	F6              string
	F7              string
	F8              string
	F9              string
	F10             string
	F11             string
	F12             string
	F13             string
}

// TrxPayment for new FieldTrxPayment initialize
func TrxPayment() FieldTrxPayment {
	return FieldTrxPayment{}
}

// InsertTrxPayment for insert data to table bw_trx_payment
func (o FieldTrxPayment) InsertTrxPayment() (*sql.DB, error) {
	db, err := ConnectDB()
	defer db.Close()

	if err != nil {
		return nil, fmt.Errorf("%s", err)
	}

	// validate if ReferenceNumber is empty
	if o.ReferenceNumber == "" {
		return nil, fmt.Errorf("ReferenceNumber not empty")
	}

	var queryInsert string
	queryInsert = `
	INSERT INTO tbl_bw_trx_payment (reference_num, payment_type, status, account, payment_number, payment_name,amount, trx_date, trx_schedule_type, third_party_name, account_type, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13) 
	values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`
	form, err := db.Prepare(queryInsert)
	if err != nil {
		return db, fmt.Errorf("failed to create form insert SQL : %v", err)
	}

	data, err := form.Exec(
		o.ReferenceNumber,
		o.PaymentType,
		o.Status,
		o.Account,
		o.PaymentNumber,
		o.PaymentName,
		o.Amount,
		Timestamp(),
		o.TrxScheduleType,
		o.ThirdPartyName,
		o.AccountType,
		o.F1,
		o.F2,
		o.F3,
		o.F4,
		o.F5,
		o.F6,
		o.F7,
		o.F8,
		o.F9,
		o.F10,
		o.F11,
		o.F12,
		o.F13,
	)
	if err != nil {
		return db, fmt.Errorf("failed insert SQL for Trx Payment : %v", err)
	}

	log.Printf("referance %s is created in Trx Payment %s", o.ReferenceNumber, data)
	return db, nil

}
