package dbtrx

import (
	"database/sql"
	"fmt"
	"log"

	// _ is driver mysql
	_ "github.com/go-sql-driver/mysql"
)

// FieldTrxLog adalah kolom yang ada pada tabel tbl_bw_trx_log
type FieldTrxLog struct {
	Username        string `json:"username"`
	Account         string `json:""`
	ReferenceNumber string `json:"reference_number"`
	TrxType         string `json:"trx_type"`
	Logged          int    `json:"logged"`
	TrxStatus       string `json:"trx_status"`
	TrxObject       string `json:"trx_object"`
	IPAddressSource string `json:"ip_address_source"`
	Agent           string `json:"agent"`
}

// TrxLog for new FieldTrxLog initialize
func TrxLog() FieldTrxLog {
	return FieldTrxLog{}
}

// InsertTrxLog for insert data to table bw_trx_log
func (o FieldTrxLog) InsertTrxLog() (*sql.DB, error) {
	db, err := ConnectDB()
	defer db.Close()

	if err != nil {
		return nil, fmt.Errorf("%s", err)
	}

	// validate if ReferenceNumber is empty
	if o.ReferenceNumber == "" {
		return nil, fmt.Errorf("ReferenceNumber not empty")
	}

	var queryInsert string
	queryInsert = `
	INSERT INTO tbl_bw_trx_log (username, account, reference_num, trx_type, logged, trx_status, trx_object, trx_date,ip_address_source, agent) 
	values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`
	form, err := db.Prepare(queryInsert)
	if err != nil {
		return db, fmt.Errorf("failed to create form insert SQL : %v", err)
	}

	data, err := form.Exec(
		o.Username,
		o.Account,
		o.ReferenceNumber,
		o.TrxType,
		o.Logged,
		o.TrxStatus,
		o.TrxObject,
		Timestamp(),
		o.IPAddressSource,
		o.Agent,
	)
	if err != nil {
		return db, fmt.Errorf("failed insert SQL for Trx Log : %v", err)
	}

	log.Printf("referance %s is created in Trx Log %s", o.ReferenceNumber, data)
	return db, nil

}
